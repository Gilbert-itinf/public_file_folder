#!/bin/bash
#top -b -n 1 -d1 | grep "Cpu" |awk '{print $2}' |awk -F. '{print $1}'

cronjob="* * * * * /home/adminuser/system_check.sh"
(crontab -u adminuser -l; echo "$cronjob" ) | crontab -u adminuser -

function check_memory(){
        date
        free -h

}
function check_cpu(){
        date
        mpstat
}
check_cpu  >> /home/adminuser/check_cpu.csv
check_memory >> /home/adminuser/check_memory.csv
